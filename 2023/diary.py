# diary -- generate printable diary PDF
# Copyright (C) 2018,2022 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only

from math import pi
import datetime
from pytz import timezone
from babel.dates import format_date
from skyfield import api
from skyfield import almanac
from skyfield import eclipselib
import cairo

# layout things

inch = 72
mm = inch / 25.4

# full bleed (inch)
paperwidth = 6.08
paperheight = 8.51

# A5
width = 148
height = 210

# borders
inner_border = 20
outer_border = 10
top_border = 20
bottom_border = 20
day_height = (height - top_border - bottom_border) / 7
day_column = day_height
sep = 2.5

# line width
thick = 1
thin = 0.25

# font size
Huge = 12.5
Large = 10
large = 7.5
normal = 5
small = 3

# font face
family = "DejaVuSans"
regular = cairo.ToyFontFace(family)
italic = cairo.ToyFontFace(family, cairo.FontSlant.OBLIQUE)
bold = cairo.ToyFontFace(family, cairo.FontSlant.NORMAL, cairo.FontWeight.BOLD)

# configuration (TODO make these command-line arguments)

filename = "diary.pdf"

year = 2023

# Royal Observatory, Greenwich
if True:
  location = api.wgs84.latlon(51.476852, -0.00050054, 45)
  zone = timezone("Europe/London")
  locale = "en_GB"
  week = "Week"
  daysize = large
  dayformat = "EEE"
  daywidth = 11
  rowskip = 2

# Sydney
if False:
  location = api.wgs84.latlon(-33.85221, 151.21062, 0)
  zone = timezone("Australia/Sydney")
  locale = "en_AU"
  week = "Week"
  daysize = large
  dayformat = "EEE"
  daywidth = 11
  rowskip = 2

# Berlin
if False:
  location = api.wgs84.latlon(52.5243700, 13.4105300, 43)
  zone = timezone("Europe/Berlin")
  locale = "de_DE"
  week = "Woche"
  daysize = normal
  dayformat = "EEE"
  daywidth = 11
  rowskip = 1.9

# Barcelona
if False:
  location = api.wgs84.latlon(41.3831, 2.1774, 0)
  zone = timezone("Europe/Madrid")
  locale = "cat"
  week = "setmana"
  daysize = normal
  dayformat = "EEEE"
  daywidth = 17
  rowskip = 3.6

# end configuration

# symbols for astronomical events
sun = "\u2609"
moon = "\u263D"
earth = "\u2295" # semantically incorrect, too small
# earth = "\u2A01" # alternative, too big
# earth = "\U0001F728" # is correct, but missing from font
up = "\u2191"
down = "\u2193"
moon_phase = ["\u25CF", "\u25D0", "\u25CB", "\u25D1" ]
# occulation = "\u206C" # "\u1F775" is proposed, filled variant
# lunar_eclipse = "\u206D" # "\u1F776" is proposed, filled variant
season_events = [ "\u2648", "\u264B", "\u264E", "\u2651" ]
# season_events = [ "Equinox", "Solstice", "Equinox", "Solstice" ]
# season_events = almanac.SEASON_EVENTS
lunar_eclipses = [ (sun + earth + moon) for e in eclipselib.LUNAR_ECLIPSES ]
solar_eclipses = [ (sun + moon + earth) for e in ["", "Total ", "Partial ", "Annular ", "Hybrid "] ]

# dates
start = datetime.datetime(year, 1, 1)
end = datetime.datetime(year + 1, 1, 1)
days_in_year = (end - start).days

# Monday on or before January 1st
start -= datetime.timedelta(days=start.weekday())
# Monday on or after January 1st
end += datetime.timedelta(days=(7 - end.weekday())%7)

# after end, to ensure `times` list is never empty when it is traversed
sentinel = end + datetime.timedelta(days=100)

times = [ ]

ts = api.load.timescale()
eph = api.load('de421.bsp')
t0 = ts.from_datetime(zone.localize(start))
t1 = ts.from_datetime(zone.localize(end))
t, y = almanac.find_discrete(t0, t1, almanac.seasons(eph))
for ti, yi in zip(t, y):
  times.append((ti, season_events[yi]))
t, y = almanac.find_discrete(t0, t1, almanac.moon_phases(eph))
for ti, yi in zip(t, y):
  times.append((ti, moon_phase[yi]))
t, y, details = eclipselib.lunar_eclipses(t0, t1, eph)
for ti, yi in zip(t, y):
  times.append((ti, lunar_eclipses[yi]))
# really location-specific but rare enough to be interesting everywhere
data = open('5MKSEcatalog.txt', 'r', encoding="ISO-8859-1")
for skip in range(11):
  data.readline()
for line in data:
  date = line[12:34]
  if date[0] == ' ':
    date = date[1:]
  y = int(date.split()[0])
  if y <= 0:
    # strptime does not like the past
    continue
  t = datetime.datetime.strptime(date + "+0000", "%Y %b %d  %H:%M:%S%z")
  if t < zone.localize(start) or zone.localize(end) < t:
    continue
  k = line[56]
  if k == 'T':
    what = 1
  elif k == 'P':
    what = 2
  elif k == 'A':
    what = 3
  elif k == 'H':
    what = 4
  else:
    what = 0
  times.append((ts.from_datetime(t), solar_eclipses[what]))
# location-specific
t, y = almanac.find_discrete(t0, t1, almanac.sunrise_sunset(eph, location))
for ti, yi in zip(t, y):
  times.append((ti, sun + (down if yi == 0 else up)))
t, y = almanac.find_discrete(t0, t1, almanac.risings_and_settings(eph, eph['Moon'], location))
for ti, yi in zip(t, y):
  times.append((ti, moon + (down if yi == 0 else up)))

times.append((ts.from_datetime(zone.localize(sentinel)), "END"))
times.sort(key=lambda x: x[0].astimezone(zone))

def nearest_minute(dt):
  return (dt + datetime.timedelta(seconds=30)).replace(second=0, microsecond=0)

surface = cairo.PDFSurface(filename, paperwidth * inch, paperheight * inch)
c = cairo.Context(surface)
c.scale(mm, mm)
c.translate((paperwidth * inch - width * mm) / 2 / mm, (paperheight * inch - height * mm) / 2 / mm)
c.set_line_cap(cairo.LineCap.ROUND)
c.set_line_join(cairo.LineJoin.ROUND)

def text_align(c, anchor_x, anchor_y, align_x, align_y, s):
  e = c.text_extents(s)
  x = anchor_x - align_x * e.width
  y = anchor_y + align_y * e.height
  c.move_to(x, y)
  c.show_text(s)

time = start
lastdst = -1 # unknown
page = "R"
caption = ""
while time < end:
  t0 = time
  t1 = time + datetime.timedelta(days=6)
  if t0.year != t1.year:
    y = str(t0.year) + "/" + str(t1.year)
  else:
    y = str(t0.year)
  if t0.month != t1.month:
    if t0.year != t1.year:
      if page == "L":
        months = format_date(t0, "LLL", locale=locale) + " " + str(t0.year) + " / " + format_date(t1, "LLL", locale=locale) + " " + str(t1.year)
      else:
        months = str(t0.year) + " " + format_date(t0, "LLL", locale=locale) + " / " + str(t1.year) + " " + format_date(t1, "LLL", locale=locale)
    else:
      if page == "L":
        months = format_date(t0, "LLL", locale=locale) + " / " + format_date(t1, "LLL", locale=locale) + " " + str(t0.year)
      else:
        months = str(t0.year) + " " + format_date(t0, "LLL", locale=locale) + " / " + format_date(t1, "LLL", locale=locale)
  else:
    if page == "L":
      months = format_date(t0, "LLLL", locale=locale) + " " + str(t0.year)
    else:
      months = str(t0.year) + " " + format_date(t0, "LLLL", locale=locale)
  w = t0.isocalendar().week
  if page == "L":
    c.set_font_face(regular)
    c.set_font_size(Large)
    text_align(c, outer_border + sep, top_border - sep * 1.5, 0, 0, months)
    c.set_line_width(thick)
    c.move_to(outer_border, top_border)
    c.line_to(width - inner_border, top_border)
    c.stroke()
    for d in range(7):
      t = time + datetime.timedelta(days=d)
      today = zone.localize(t)
      tomorrow = zone.localize(t + datetime.timedelta(days=1))
      dow = t.strftime("%A")
      dom = t.day
      doy = t.timetuple().tm_yday
      c.set_font_face(bold)
      c.set_font_size(Huge)
      text_align(c, outer_border + day_column / 2, top_border + sep + day_height * d, 0.5, 1, str(dom))
      c.set_font_face(regular)
      c.set_font_size(daysize)
      text_align(c, outer_border + day_column / 2, top_border + sep + day_height * (d + 2/4), 0.5, 0.6, format_date(t, dayformat, locale=locale))
      c.set_font_face(regular)
      c.set_font_size(small)
      text_align(c, outer_border + day_column / 2, top_border - sep + day_height * (d + 1), 0.5, 0, "(" + str(doy) + "-" + str(days_in_year - doy) + ")")
      s = ""
      while times[0][0].astimezone(zone) < tomorrow:
        # print with timezone only when DST changes
        if (times[0][0].astimezone(zone).dst() == today.dst() and times[1][0].astimezone(zone).dst() != today.dst()) or (lastdst != times[0][0].astimezone(zone).dst()):
          s = s + nearest_minute(times[0][0].astimezone(zone)).strftime("%H:%M%Z") + times[0][1]
        else:
          s = s + nearest_minute(times[0][0].astimezone(zone)).strftime("%H:%M") + times[0][1]
        lastdst = times[0][0].astimezone(zone).dst()
        times.pop(0)
      c.set_font_face(regular)
      c.set_font_size(small)
      text_align(c, width - inner_border - sep, top_border + day_height * d + sep, 1, 1, s)
      if d == 4 or d == 6:
        c.set_line_width(thick)
      else:
        c.set_line_width(thin)
      c.move_to(outer_border, top_border + day_height * (d + 1))
      c.line_to(width - inner_border, top_border + day_height * (d + 1))
      c.stroke()
    c.set_font_face(regular)
    c.set_font_size(small)
    text_align(c, outer_border + day_column / 2, top_border + day_height * 7 + sep, 0.5, 1, week)
    text_align(c, outer_border + day_column / 2, top_border + day_height * 7 + sep + normal, 0.5, 1, str(w))
    c.show_page()
    page = "R"
  else:
    c.set_font_face(regular)
    c.set_font_size(Large)
    text_align(c, width - outer_border - sep, top_border - sep * 1.5, 1, 0, months)
    c.set_line_width(thick)
    c.move_to(width - outer_border, top_border)
    c.line_to(inner_border, top_border)
    c.stroke()
    for d in range(7):
      t = time + datetime.timedelta(days=d)
      today = zone.localize(t)
      tomorrow = zone.localize(t + datetime.timedelta(days=1))
      dow = t.strftime("%A")
      dom = t.day
      doy = t.timetuple().tm_yday
      c.set_font_face(bold)
      c.set_font_size(Huge)
      text_align(c, width - outer_border - day_column / 2, top_border + sep + day_height * d, 0.5, 1, str(dom))
      c.set_font_face(regular)
      c.set_font_size(daysize)
      text_align(c, width - outer_border - day_column / 2, top_border + sep + day_height * (d + 2/4), 0.5, 0.6, format_date(t, dayformat, locale=locale))
      c.set_font_face(regular)
      c.set_font_size(small)
      text_align(c, width - outer_border - day_column / 2, top_border - sep + day_height * (d + 1), 0.5, 0, "(" + str(doy) + "-" + str(days_in_year - doy) + ")")
      s = ""
      while times[0][0].astimezone(zone) < tomorrow:
        # print with timezone only when DST changes
        if (times[0][0].astimezone(zone).dst() == today.dst() and times[1][0].astimezone(zone).dst() != today.dst()) or (lastdst != times[0][0].astimezone(zone).dst()):
          s = s + nearest_minute(times[0][0].astimezone(zone)).strftime("%H:%M%Z") + times[0][1]
        else:
          s = s + nearest_minute(times[0][0].astimezone(zone)).strftime("%H:%M") + times[0][1]
        lastdst = times[0][0].astimezone(zone).dst()
        times.pop(0)
      c.set_font_face(regular)
      c.set_font_size(small)
      text_align(c, inner_border + sep, top_border + day_height * d + sep, 0, 1, s)
      if d == 4 or d == 6:
        c.set_line_width(thick)
      else:
        c.set_line_width(thin)
      c.move_to(inner_border, top_border + day_height * (d + 1))
      c.line_to(width - outer_border, top_border + day_height * (d + 1))
      c.stroke()
    c.set_font_face(regular)
    c.set_font_size(small)
    text_align(c, width - outer_border - day_column / 2, top_border + day_height * 7 + sep, 0.5, 1, week)
    text_align(c, width - outer_border - day_column / 2, top_border + day_height * 7 + sep + normal, 0.5, 1, str(w))
    c.show_page()
    page = "L"
  time += datetime.timedelta(days=7)
#print("latitude", str(location.latitude.degrees) + "\\degree\n")
#print("longitude", str(location.longitude.degrees) + "\\degree\n")
#print("elevation", str(location.elevation.m) + "m\n")
#print("time zone", zone, "\n")
#print("locale", latex(locale), "\n")
#print("""
#\\vspace*{\\fill}
#\\centering
#\\normalsize mathr.co.uk/diary
#""")
surface.finish()
