# diary

generate printable diary PDF

## 2023

```
sudo apt install python3-babel python3-sgp4 python3-numpy python3-certifi python3-pip
pip install skyfield
wget "https://eclipse.gsfc.nasa.gov/5MCSE/5MKSEcatalog.txt"
python3 diary.py
# output in diary.pdf
```

Note: skyfield will download data sets from the internet on first run of
`diary.py`.

## legal

diary -- generate printable diary PDF

Copyright (C) 2018,2022  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

---
https://mathr.co.uk/diary
