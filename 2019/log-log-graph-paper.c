/*
diary -- generate printable diary PDF
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#include <stdio.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLSL(s) #s

static const int width = 3648;
static const int height = 5106;
static const float size = 5.0;

static const char *frag_src = GLSL(

  const float pi = 3.141592653589793;

  float normal_pdf(float x) { return exp(-x*x/2.0) / sqrt(2.0 * pi); }
  float normal_cdf(float x) { // Zelen & Severo (1964)
    const float b0 = 0.2316419;
    const float b1 = 0.319381530;
    const float b2 = -0.356563782;
    const float b3 = 1.781477937;
    const float b4 = -1.821255978;
    const float b5 = 1.330274429;
    float t = 1.0 / (1.0 + b0 * x);
    return 1.0 - normal_pdf(x) * (t * (b1 + t * (b2 + t * (b3 + t * (b4 + t * b5)))));
  }

  uniform vec2 base;

  void main() {
    vec2 q = gl_TexCoord[0].xy;
    q *= mat2(0.0, -1.0, 1.0, 0.0);
    float d = length(vec4(dFdx(q), dFdy(q)));
    vec2 q0 = q;
    q -= floor(q);
    vec2 b = pow(base, q);
    b = floor(b);
    b = log(b) / log(base);
    if (base.x == 1.0) b.x = floor(8.0 * q.x) / 8.0;
    if (base.x == 1.0 && base.y == 1.0) { q.y = 4.0 * normal_cdf(abs(q0.y)); d = length(vec4(dFdx(q), dFdy(q))); }
    if (base.y == 1.0) b.y = floor(8.0 * q.y) / 8.0;
    vec2 e = abs(q - b);
    float f = min(min(e.x, e.y), min(1.0 - e.x, 1.0 - e.y));
    gl_FragColor = vec4(vec3(0.5 + 0.5 * smoothstep(2.0 * d, 4.0 * d, f)), 1.0);
  }

);

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

int main(int argc, char **argv) {

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("graphpaper");
  glewInit();

  GLint success;
  int prog = glCreateProgram();
  int frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, (const GLchar **) &frag_src, 0);
  glCompileShader(frag);
  glAttachShader(prog, frag);
  glLinkProgram(prog);
  glGetProgramiv(prog, GL_LINK_STATUS, &success);
  if (!success)
  {
    debug_shader(frag, GL_FRAGMENT_SHADER, frag_src);
    debug_program(prog);
    exit(1);
  }
  glUseProgram(prog);
  GLuint ubase = glGetUniformLocation(prog, "base");

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

  glViewport(0, 0, width, height);
  glLoadIdentity();
  gluOrtho2D(0, 1, 1, 0);

  unsigned char *buffer = malloc(width * height);
  float base[4][2] = { { 1, 1 }, { 1, 10 }, { 10, 1 }, { 10, 10 } };
  for (int k = 0; k < 4; ++k) {
        glUniform2fv(ubase, 1, &base[k][0]);
        glBegin(GL_QUADS); {
          float u = size / 2.0;
          float w = u * height / width / 0.825223;
          glTexCoord2f( u / 0.889977, -w); glVertex2f(1, 0);
          glTexCoord2f( u / 0.889977,  w); glVertex2f(1, 1);
          glTexCoord2f(-u / 0.760470,  w); glVertex2f(0, 1);
          glTexCoord2f(-u / 0.760470, -w); glVertex2f(0, 0);
        } glEnd();
        glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, buffer);
        char fname[200];
        snprintf(fname, 100, "log-log-%d.pgm", k);
        FILE *f = fopen(fname, "wb");
        fprintf(f, "P5\n%d %d\n255\n", width, height);
        fflush(f);
        fwrite(buffer, width * height, 1, f);
        fflush(f);
        fclose(f);
  }

  free(buffer);
  glDeleteFramebuffers(1, &fbo);
  glDeleteTextures(1, &tex);
  glDeleteShader(frag);
  glDeleteProgram(prog);
  glutReportErrors();
  return 0;
}
