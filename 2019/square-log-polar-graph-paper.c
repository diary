/*
diary -- generate printable diary PDF
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#include <stdio.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLSL(s) #s

static const int width = 3648;
static const int height = 5106;
static const float size = 4.0;

static const char *frag_src = GLSL(

  uniform vec4 twist;
  uniform bool loxodrome;

  vec2 clog(vec2 x) {
    return vec2(log(length(x)), atan(x.y, x.x));
  }

vec2 cMul(vec2 a, vec2 b) {
	return vec2( a.x*b.x -  a.y*b.y,a.x*b.y + a.y * b.x);
}

vec2 cConj(vec2 a)
{
  return vec2(a.x, -a.y);
}

vec2 cDiv(vec2 a, vec2 b)
{
  return cMul(a, cConj(b)) / dot(b, b);
}

  vec4 clog(vec4 x)
  {
    return vec4(clog(x.xy), cDiv(x.zw, x.xy)) * 0.15915494309189535;
  }
  
  vec3 stereo(vec2 x) {
    return vec3(2.0 * x, dot(x, x) - 1.0) / (dot(x, x) + 1.0);
  }
  
  vec2 unstereo(vec3 x) {
    return x.xy / (1.0 - x.z);
  }
  
  vec3 rotate(vec3 x) {
    return x.zyx * vec3(1.0, 1.0, -1.0);
  }
  
  void main() {
    float factor = twist.w;
    vec2 p = gl_TexCoord[0].xy;
    p *= mat2(0.0, -1.0, 1.0, 0.0);
    if (loxodrome)
    {
      p = unstereo(rotate(stereo(p)));
    }
    vec4 q4 = clog(vec4(p, length(vec4(dFdx(p), dFdy(p))), 0.0));
    float a = atan(twist.y, twist.x);
    float h = length(vec2(twist.x, twist.y));
    q4.xy *= mat2(cos(a), sin(a), -sin(a), cos(a)) * h;
    q4.zw *= mat2(cos(a), sin(a), -sin(a), cos(a)) * h;
    float d = length(q4.zw);
    float l = ceil(-log(d)/log(factor));
    float f = l + log(d)/log(factor);
    l -= factor < 2.5 ? 9.0 : 5.0;
    float o[2];
    for (int i = 0; i < 2; ++i) {
      l += 1.0;
      vec2 u = q4.xy * pow(factor, l);
      u *= twist.z / pow(factor, 2.0);
      u -= floor(u);
      float r = min
        ( min(length(u), length(u - vec2(1.0, 0.0)))
        , min(length(u - vec2(0.0, 1.0)), length(u - vec2(1.0, 1.0)))
        );
      float c = clamp(pow(2.0, factor)/8.0 * r / (pow(factor, l) * d), 0.0, 1.0);
      vec2 v = q4.xy * pow(factor, l - 1.0);
      v *= twist.z / pow(factor, 2.0);
      v -= floor(v);
      float s = min(min(v.x, v.y), min(1.0 - v.x, 1.0 - v.y));
      float k = clamp(pow(2.0, factor)/8.0 * s / (pow(factor, l - 1.0) * d), 0.0, 1.0);
      o[i] = c * k;
    }
    gl_FragColor = vec4(vec3(0.5 + 0.5 * mix(o[1], o[0], f)), 1.0);
  }

);

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

int main(int argc, char **argv) {

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("graphpaper");
  glewInit();

  GLint success;
  int prog = glCreateProgram();
  int frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, (const GLchar **) &frag_src, 0);
  glCompileShader(frag);
  glAttachShader(prog, frag);
  glLinkProgram(prog);
  glGetProgramiv(prog, GL_LINK_STATUS, &success);
  if (!success)
  {
    debug_shader(frag, GL_FRAGMENT_SHADER, frag_src);
    debug_program(prog);
    exit(1);
  }
  glUseProgram(prog);
  GLuint utwist = glGetUniformLocation(prog, "twist");
  GLuint uloxodrome = glGetUniformLocation(prog, "loxodrome");

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

  glViewport(0, 0, width, height);
  glLoadIdentity();
  gluOrtho2D(0, 1, 1, 0);

  unsigned char *buffer = malloc(width * height);
  float twist[8][4] =
    { { 1, 0, 8, 2 }
    , { 1, 1, 8, 2 }
    , { 2, 1, 8, 2 }
    , { 3, 2, 8, 2 }
    , { 1, 0, 6, 3 }
    , { 1, 1, 6, 3 }
    , { 2, 1, 6, 3 }
    , { 3, 2, 6, 3 }
    };
  for (int k = 0; k < 8; ++k) {
    for (int l = 1; l < 2; ++l) {
        glUniform4fv(utwist, 1, &twist[k][0]);
        glUniform1i(uloxodrome, l);
        glBegin(GL_QUADS); {
          float u = size / 2.0;
          float w = u * height / width / 0.825223;
          glTexCoord2f( u / 0.889977, -w); glVertex2f(1, 0);
          glTexCoord2f( u / 0.889977,  w); glVertex2f(1, 1);
          glTexCoord2f(-u / 0.760470,  w); glVertex2f(0, 1);
          glTexCoord2f(-u / 0.760470, -w); glVertex2f(0, 0);
        } glEnd();
        glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, buffer);
        char fname[200];
        snprintf(fname, 100, "square-%d-%d.pgm", k, l);
        FILE *f = fopen(fname, "wb");
        fprintf(f, "P5\n%d %d\n255\n", width, height);
        fflush(f);
        fwrite(buffer, width * height, 1, f);
        fflush(f);
        fclose(f);
    }
  }

  free(buffer);
  glDeleteFramebuffers(1, &fbo);
  glDeleteTextures(1, &tex);
  glDeleteShader(frag);
  glDeleteProgram(prog);
  glutReportErrors();
  return 0;
}
