/*
diary -- generate printable diary PDF
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

// gcc -std=c99 -Wall -Wextra -pedantic -O3 -o cover cover.c -lgsl -lgslcblas -lm
// ./cover W H C N F > cover.ppm

// popen()
#define _POSIX_C_SOURCE 2

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gsl/gsl_sf_zeta.h>

/*
// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl\n"
vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
*/
double fract(double x) { return x - floor(x); }
double clamp(double x, double mi, double ma) { return fmax(mi, fmin(x, ma)); }
double mix(double a, double b, double x) { return a * (1 - x) + x * b; }
void hsv2rgb(double *r, double *g, double *b, double h, double s, double v)
{
  double K[4] = { 1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0 };
  double p[3] = { fabs(fract(h + K[0]) * 6 - K[3]), fabs(fract(h + K[1]) * 6 - K[3]), fabs(fract(h + K[2]) * 6 - K[3]) };
  double q[3] = { clamp(p[0] - K[0], 0.0, 1.0), clamp(p[1] - K[0], 0.0, 1.0), clamp(p[2] - K[0], 0.0, 1.0) };
  double c[3] = { v * mix(K[0], q[0], s), v * mix(K[0], q[1], s), v * mix(K[0], q[2], s) };
  *r = c[0];
  *g = c[1];
  *b = c[2];
}

struct histogram
{
  int64_t levels;
  unsigned char **counts;
};

static inline void h_set(struct histogram *h, int64_t l, int64_t y, int64_t x, uint64_t v)
{
  int64_t d = 1LL << l;
  int64_t bytes_per_side = (d + 7) >> 3;
  int64_t bytes = d * bytes_per_side;
  int64_t byte = x >> 3;
  int64_t bit  = x & 7;
  int64_t mask = ~(1 << bit);
  int64_t k0 = y * bytes_per_side + byte;
  int64_t bs = 2 * (h->levels - l) + 1;
  for (int64_t b = 0; b < bs; ++b)
  {
    int64_t k = b * bytes + k0;
    h->counts[l][k] &= mask;
    h->counts[l][k] |= ((v >> b) & 1) << bit;
  }
}

static inline uint64_t h_get(struct histogram *h, int64_t l, int64_t y, int64_t x)
{
  int64_t d = 1LL << l;
  int64_t bytes_per_side = (d + 7) >> 3;
  int64_t bytes = d * bytes_per_side;
  int64_t byte = x >> 3;
  int64_t bit  = x & 7;
  int64_t mask = (1 << bit);
  int64_t k0 = y * bytes_per_side + byte;
  int64_t bs = 2 * (h->levels - l) + 1;
  uint64_t v = 0;
  for (int64_t b = 0; b < bs; ++b)
  {
    int64_t k = b * bytes + k0;
    v |= ((h->counts[l][k] & mask) >> bit) << b;
  }
  return v;
}

struct histogram *h_new(int64_t width, int64_t height)
{
  int64_t levels = 0;
  int64_t size = 1LL << levels;
  while (size < width || size < height)
  {
    levels += 1;
    size <<= 1;
  }
  struct histogram *h = malloc(sizeof(struct histogram));
  h->levels = levels;
  h->counts = malloc(sizeof(char *) * (levels + 1));
  for (int64_t l = 0; l <= levels; ++l)
  {
    int64_t d = 1LL << l;
    int64_t bytes_per_side = (d + 7) / 8;
    int64_t bytes = d * bytes_per_side;
    int64_t bits = 2 * (levels - l) + 1;
    h->counts[l] = malloc(bits * bytes);
  }
  for (int64_t y = 0; y < size; ++y)
    for (int64_t x = 0; x < size; ++x)
      h_set(h, levels, y, x, y < height && x < width);
  for (int64_t l = levels - 1; l >= 0; --l)
    for (int64_t y = 0; y < 1 << l; ++y)
      for (int64_t x = 0; x < 1 << l; ++x)
        h_set(h, l, y, x,
            h_get(h, l + 1, (y << 1) + 0, (x << 1) + 0)
          + h_get(h, l + 1, (y << 1) + 0, (x << 1) + 1)
          + h_get(h, l + 1, (y << 1) + 1, (x << 1) + 0)
          + h_get(h, l + 1, (y << 1) + 1, (x << 1) + 1)
        );
  assert(h_get(h, 0, 0, 0) == (uint64_t) width * height);
  return h;
}

void h_free(struct histogram *h)
{
  for (int64_t l = 0; l <= h->levels; ++l)
    free(h->counts[l]);
  free(h->counts);
  free(h);
}

void h_decrement(struct histogram *h, int64_t x, int64_t y)
{
  for (int64_t l = h->levels; l >= 0; --l)
  {
    uint64_t v = h_get(h, l, y, x);
    if (v > 0) { v -= 1; }
    else {
      assert(l == h->levels);
      break;
    }
    h_set(h, l, y, x, v);
    x >>= 1;
    y >>= 1;
  }
}

int h_empty(struct histogram *h)
{
  return h_get(h, 0, 0, 0) == 0;
}

int h_choose(struct histogram *h, int64_t *x, int64_t *y)
{
  if (h_empty(h)) return 0;
  *x = 0;
  *y = 0;
  for (int64_t l = 1; l <= h->levels; ++l)
  {
    *x <<= 1;
    *y <<= 1;
    int64_t xs[4] = { *x, *x + 1, *x, *x + 1 };
    int64_t ys[4] = { *y, *y, *y + 1, *y + 1 };
    uint64_t ss[4] =
      { h_get(h, l, ys[0], xs[0])
      , h_get(h, l, ys[1], xs[1])
      , h_get(h, l, ys[2], xs[2])
      , h_get(h, l, ys[3], xs[3])
      };
    uint64_t ts[4] =
      { ss[0]
      , ss[0] + ss[1]
      , ss[0] + ss[1] + ss[2]
      , ss[0] + ss[1] + ss[2] + ss[3]
      };
    uint64_t p = rand() % ts[3];
    int i;
    for (i = 0; i < 4; ++i)
      if (p < ts[i]) break;
    *x = xs[i];
    *y = ys[i];
  }
  return 1;
}

struct image {
  int64_t width;
  int64_t height;
  int64_t bytes_per_line;
  unsigned char *data;
};

struct image *i_new(int64_t width, int64_t height)
{
  struct image *i = calloc(1, sizeof(struct image));
  i->width = width;
  i->height = height;
  i->bytes_per_line = (width + 3) >> 2;
  i->data = calloc(1, i->bytes_per_line * height);
  return i;
}

void i_set(struct image *i, int64_t y, int64_t x, uint64_t v)
{
  int64_t byte = i->bytes_per_line * y + (x >> 2);
  int64_t bit = (x & 3) << 1;
  int64_t mask = ~(3 << bit);
  i->data[byte] &= mask;
  i->data[byte] |= (v & 3) << bit;
}

uint64_t i_get(const struct image *i, int64_t y, int64_t x)
{
  int64_t byte = i->bytes_per_line * y + (x >> 2);
  int64_t bit = (x & 3) << 1;
  int64_t mask = (3 << bit);
  return (i->data[byte] & mask) >> bit;
}

struct image *image;

struct histogram *initialize(int64_t width, int64_t height)
{
  image = i_new(width, height);
  return h_new(width, height);
}

double rectangle_radius(double ai, double aspect) { double t = atan(aspect); return sqrt(ai / (4 * cos(t) * sin(t))); }

unsigned char gpgm[2][4096][4096];
double gx, gy, gr, ga, gf;

void load_glyph(void)
{
    FILE *f = popen("convert mask.png -background white -alpha remove pgm:-", "r");
    fscanf(f, "P5\n4096 4096\n255");
    assert(fgetc(f) == '\n');
    fread(&gpgm[0][0][0], 4096 * 4096, 1, f);
    pclose(f);
    f = popen("convert shape.png -background white -alpha remove pgm:-", "r");
    fscanf(f, "P5\n4096 4096\n255");
    assert(fgetc(f) == '\n');
    fread(&gpgm[1][0][0], 4096 * 4096, 1, f);
    pclose(f);
    // compute glyph bounds
    int minx = 4095, miny = 4095, maxx = 0, maxy = 0, w = 0;
    for (int y = 0; y < 4096; ++y)
    for (int x = 0; x < 4096; ++x)
    if (gpgm[0][y][x] == 0)
    {
      minx = x < minx ? x : minx;
      miny = y < miny ? y : miny;
      maxx = x > maxx ? x : maxx;
      maxy = y > maxy ? y : maxy;
      ++w;
    }
    gx = maxx - minx;
    gy = maxy - miny;
    gr = 0.5 * hypot(gx, gy);
    ga = gx / gy;
    gx *= 0.5;
    gy *= 0.5;
    gx += minx;
    gy += miny;
    gf = 3.141592653589793 * gr * gr / w;
}

int draw_glyph(int draw, struct histogram *h, int width, int height, double cx, double cy, double r, double co, double si, int c, int b, int s)
{
  double r2 = r * r;
  int64_t x0 = floor(cx - r);
  int64_t x1 = ceil (cx + r);
  int64_t y0 = floor(cy - r);
  int64_t y1 = ceil (cy + r);
  for (int64_t y = y0; y <= y1; ++y)
  {
    for (int64_t x = x0; x <= x1; ++x)
    {
      double dx = x - cx;
      double dy = y - cy;
      double d2 = dx * dx + dy * dy;
      if (d2 <= r2)
      {
        int64_t ix = gx + (co * dx - si * dy) * gr / r;
        int64_t iy = gy + (si * dx + co * dy) * gr / r;
        if (0 <= ix && ix < 4096 && 0 <= iy && iy < 4096 && gpgm[0][iy][ix] == 0)
        {
          if (draw)
          {
            int64_t u = x, v = y;
            if (0 <= u && u < width && 0 <= v && v < height) { h_decrement(h, u, v); i_set(image, v, u, c); }
            if (u < 2 * b - s) u += width - (2 * b - s);
            if (0 <= u && u < width && 0 <= v && v < height) { h_decrement(h, u, v); i_set(image, v, u, c); }
            if (u >= width - (2 * b - s)) u -= width - (2 * b - s);
            if (0 <= u && u < width && 0 <= v && v < height) { h_decrement(h, u, v); i_set(image, v, u, c); }
            if (v < 2 * b - s) { u -= width / 2; u = -u; u += width / 2; v -= b - s / 2; v = -v; v += b - s / 2; }
            if (0 <= u && u < width && 0 <= v && v < height) { h_decrement(h, u, v); i_set(image, v, u, c); }
            if (v >= height - (2 * b - s)) { u -= width / 2; u = -u; u += width / 2; v -= height - (b - s / 2); v = -v; v += height - (b - s / 2); }
            if (0 <= u && u < width && 0 <= v && v < height) { h_decrement(h, u, v); i_set(image, v, u, c); }
          }
          else
          {
            int64_t u = x, v = y;
            if (0 <= u && u < width && 0 <= v && v < height && i_get(image, v, u)) return 0;
            if (u < 2 * b - s) u += width - (2 * b - s);
            if (0 <= u && u < width && 0 <= v && v < height && i_get(image, v, u)) return 0;
            if (u >= width - (2 * b - s)) u -= width - (2 * b - s);
            if (0 <= u && u < width && 0 <= v && v < height && i_get(image, v, u)) return 0;
            if (v < 2 * b - s) { u -= width / 2; u = -u; u += width / 2; v -= b - s / 2; v = -v; v += b - s / 2; }
            if (0 <= u && u < width && 0 <= v && v < height && i_get(image, v, u)) return 0;
            if (v >= height - (2 * b - s)) { u -= width / 2; u = -u; u += width / 2; v -= height - (b - s / 2); v = -v; v += height - (b - s / 2); }
            if (0 <= u && u < width && 0 <= v && v < height && i_get(image, v, u)) return 0;
          }
        }
      }
    }
  }
  return 1;
}

double area_factor(double A, double c, double N)
{
  return A / gsl_sf_hzeta(c, N);
}

double area_i(double Af, double c, double N, int i)
{
  return Af / pow(i + N, c);
}

void packing(struct histogram *h, int width, int height, double c, double N, double fill)
{
  int64_t i = 1;
  int border = 170;
  int spine = 290;
  draw_glyph(1, h, width, height, width - height / 2 / ga - spine, height / 2, height / 2, 0, 1, 1, border, spine);
  double r1 = spine * 4 * 2./3 / ga;
  int w = r1 * 2 * 5./4;
  for (int y1 = ((height / 2) % w); y1 < height; y1 += w)
    draw_glyph(1, h, width, height, width / 2, y1, r1, 0, -1, 1, border, spine);
  double A = (width - 2 * border) * (height - 2 * border) - (height / 2) * (height / 2) / gf;
  double Af = area_factor(A, c, N);
  double a = 0;
  time_t last = time(0);
  while (a < fill * A && ! h_empty(h))
  {
    double Ai = area_i(Af, c, N, i++);
    a += Ai;
    double r = rectangle_radius(Ai * gf, ga);
    if (r < 1) break;
    uint64_t jj = sqrt(h_get(h, 0, 0, 0));
    for (uint64_t j = 0; j < jj; ++j)
    {
      time_t now = time(0);
      if (now >= last + 1)
      {
        fprintf(stderr, "%16ld\t%f\t%f\t%16lu\r", i, a / (fill * A), r, j);
        last = now;
      }
      int64_t x = -1, y = -1;
      if (h_choose(h, &x, &y))
      {
        double t = rand() / (double) RAND_MAX * 6.283185307179586;
        double co = cos(t);
        double si = sin(t);
        if (draw_glyph(0, h, width, height, x, y, r, co, si, (i % 2) + 2, border, spine))
        {
          draw_glyph(1, h, width, height, x, y, r, co, si, (i % 2) + 2, border, spine);
          break;
        }
      }
      else
      {
        break;
      }
    }
  }
  fprintf(stderr, "\n");
}

void write_ppm(int64_t width, int64_t height, int64_t seed)
{
  double hue = 0.9;
  double sat = 1;
  double val = 1;
  double red, grn, blu;
  hsv2rgb(&red, &grn, &blu, hue, sat, val);
  unsigned char rc = 255 * red;
  unsigned char gc = 255 * grn;
  unsigned char bc = 255 * blu;
  unsigned char rgb[4][3] = { { rc, gc, bc }, { rc, gc, bc }, { 255, 255, 255 }, { 0, 0, 0 } };
  unsigned char *scanline = malloc(3 * width);
  fprintf(stdout, "P6\n%ld %ld\n# %ld\n255\n", width, height, seed);
  for (int64_t y = 0; y < height; ++y)
  {
    for (int64_t x = 0; x < width; ++x)
    {
      uint64_t c = i_get(image, y, x) & 3;
      scanline[3 * x + 0] = rgb[c][0];
      scanline[3 * x + 1] = rgb[c][1];
      scanline[3 * x + 2] = rgb[c][2];
    }
    fwrite(scanline, width * 3, 1, stdout);
  }
  fflush(stdout);
}

int main(int argc, char **argv)
{
  if (argc < 6) return 1;
  int width = atoi(argv[1]);
  int height = atoi(argv[2]);
  uint64_t npixels = (uint64_t) width * height;
  assert(npixels < 1LU << 31LU);
  double c = atof(argv[3]);
  double N = atof(argv[4]);
  double fill = atof(argv[5]);
  uint64_t seed = time(0);
  if (argc > 6) seed = atol(argv[6]);
  srand(seed);
  struct histogram *h = initialize(width, height);
  load_glyph();
  packing(h, width, height, c, N, fill);
  write_ppm(width, height, seed);
  return 0;
}
