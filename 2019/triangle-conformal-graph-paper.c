/*
diary -- generate printable diary PDF
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#include <math.h>
#include <stdio.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLSL(s) "#define REAL float\n" "#define VEC2 vec2\n" "#define VEC4 vec4\n" #s

static const int width = 3648;
static const int height = 5106;
static const float size = 4.0;


static const char *frag_src = GLSL(

  const float pi = 3.141592653589793;
  uniform float factor;
  uniform int formula;

  vec2 crecip(vec2 x) {
    return vec2(x.x, -x.y) / dot(x, x);
  }

VEC2 cExp(VEC2 z) {
	return exp(z.x) * VEC2(cos(z.y), sin(z.y));
}

REAL cAbs(VEC2 z) {
	return length(z);
}

VEC2 cAdd( VEC2 a, REAL s ) {
  return VEC2( a.x+s, a.y );
}

VEC2 cAdd( REAL s, VEC2 a ) {
  return VEC2( s+a.x, a.y );
}

VEC2 cAdd( VEC2 a, VEC2 s ) {
  return a + s;
}

VEC2 cSub( VEC2 a, REAL s ) {
  return VEC2( a.x-s, a.y );
}

VEC2 cSub( REAL s, VEC2 a ) {
  return VEC2( s-a.x, -a.y );
}

VEC2 cSub( VEC2 a, VEC2 s ) {
  return a - s;
}

REAL cArg(VEC2 a) {
	return atan(a.y,a.x);
}

VEC2 cSqr(VEC2 z) {
	return VEC2(z.x*z.x-z.y*z.y,2.*z.x*z.y);
}

VEC2 cMul(VEC2 a, VEC2 b) {
	return VEC2( a.x*b.x -  a.y*b.y,a.x*b.y + a.y * b.x);
}

vec2 cConj(vec2 a)
{
  return vec2(a.x, -a.y);
}

vec2 cDiv(vec2 a, vec2 b)
{
  return cMul(a, cConj(b)) / dot(b, b);
}

vec4 cExp(vec4 a)
{
  return vec4(cExp(a.xy), cMul(a.zw, cExp(a.xy)));
}

vec4 cSqr(vec4 a)
{
  return vec4(cSqr(a.xy), 2.0 * cMul(a.xy, a.zw));
}

vec4 cMul(vec4 a, vec4 b)
{
  return vec4(cMul(a.xy, b.xy), cMul(a.xy, b.zw) + cMul(a.zw, b.xy));
}

vec2 cSqrt( vec2 z ) {
  float m = length(z);
  return sqrt( max(vec2(0.0), 0.5*vec2(m+z.x, m-z.x)) ) *
    vec2( 1.0, sign(z.y) );
}

vec4 cSqrt( vec4 z ) {
  return vec4(cSqrt(z.xy), 0.5 * cDiv(z.zw, cSqrt(z.xy)));
}

vec2 cLog(vec2 a)
{
  return vec2(log(cAbs(a.xy)), cArg(a.xy));
}

VEC4 cLog(VEC4 a) {
	return VEC4(cLog(a.xy), cDiv(a.zw, a.xy));
}

VEC4 cAsin(VEC4 z) {
  const VEC4 I = VEC4(0.0, 1.0, 0.0, 0.0);
  return cMul(-I, cLog(cMul(I, z) + cSqrt(vec4(1.0, vec3(0.0)) - cSqr(z))));
}

VEC2 cPow(VEC2 z, VEC2 a) {
	return cExp(cMul(cLog(z), a));
}

VEC4 cPow(VEC4 z, REAL a) {
	return cExp(cLog(z) * a);
}

  void main() {
    vec2 p = gl_TexCoord[0].xy;
    if (formula == 0) p *= mat2(0.0, -1.0, 1.0, 0.0);
    float d = length(vec4(dFdx(p), dFdy(p)));
    vec4 q4 = vec4(p, d, 0.0);
    if (formula == 0) q4 = cAsin(q4) / pi * sqrt(3.0)/2.0;
    if (formula == 1) q4 = cSqrt(q4);
    if (formula == 2) q4 = cPow(q4, 1.0/3.0);
    vec2 q = q4.xy;
    d = length(q4.zw);
    q.x /= sqrt(3.0) / 2.0;
    q.y += q.x / 2.0;
    float l = ceil(-log(d)/log(factor));
    float f = l + log(d)/log(factor);
    l -= factor < 2.5 ? 9.0 : 5.0;
    float o[2];
    for (int i = 0; i < 2; ++i) {
      l += 1.0;
      vec2 u = q * pow(factor, l);
      u *= (factor < 2.5 ? 8.0 : 6.0) / pow(factor, 2.0);
      u -= floor(u);
      float r = min
        ( min(length(u), length(u - vec2(1.0, 0.0)))
        , min(length(u - vec2(0.0, 1.0)), length(u - vec2(1.0, 1.0)))
        );
      float c = clamp(pow(2.0, factor)/8.0 * r / (pow(factor, l) * d), 0.0, 1.0);
      vec2 v = q * pow(factor, l - 1.0);
      v *= (factor < 2.5 ? 8.0 : 6.0) / pow(factor, 2.0);
      v -= floor(v);
      float s = min(min(min(v.x, v.y), min(1.0 - v.x, 1.0 - v.y)), abs(v.x - v.y));
      float k = clamp(pow(2.0, factor)/8.0 * s / (pow(factor, l - 1.0) * d), 0.0, 1.0);
      o[i] = c * k;
    }
    gl_FragColor = vec4(vec3(0.5 + 0.5 * mix(o[1], o[0], f)), 1.0);
  }

);

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

int main(int argc, char **argv) {

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("graphpaper");
  glewInit();

  GLint success;
  int prog = glCreateProgram();
  int frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, (const GLchar **) &frag_src, 0);
  glCompileShader(frag);
  glAttachShader(prog, frag);
  glLinkProgram(prog);
  glGetProgramiv(prog, GL_LINK_STATUS, &success);
  if (!success)
  {
    debug_shader(frag, GL_FRAGMENT_SHADER, frag_src);
    debug_program(prog);
    exit(1);
  }
  glUseProgram(prog);
  GLuint ufactor = glGetUniformLocation(prog, "factor");
  GLuint uformula = glGetUniformLocation(prog, "formula");

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

  glViewport(0, 0, width, height);
  glLoadIdentity();
  gluOrtho2D(0, 1, 1, 0);

  unsigned char *buffer = malloc(width * height);
  for (int k = 2; k <= 3; ++k) {
    for (int l = 0; l <= 2; ++l) {
        glUniform1f(ufactor, k);
        glUniform1i(uformula, l);
        glBegin(GL_QUADS); {
          float u = size / 2.0;
          float w = u * height / width / 0.825223;
          glTexCoord2f( u / 0.889977, -w); glVertex2f(1, 0);
          glTexCoord2f( u / 0.889977,  w); glVertex2f(1, 1);
          glTexCoord2f(-u / 0.760470,  w); glVertex2f(0, 1);
          glTexCoord2f(-u / 0.760470, -w); glVertex2f(0, 0);
        } glEnd();
        glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, buffer);
        char fname[200];
        snprintf(fname, 100, "triangle-conformal-%d-%d.pgm", k, l);
        FILE *f = fopen(fname, "wb");
        fprintf(f, "P5\n%d %d\n255\n", width, height);
        fflush(f);
        fwrite(buffer, width * height, 1, f);
        fflush(f);
        fclose(f);
    }
  }

  free(buffer);
  glDeleteFramebuffers(1, &fbo);
  glDeleteTextures(1, &tex);
  glDeleteShader(frag);
  glDeleteProgram(prog);
  glutReportErrors();
  return 0;
}
