/*
diary -- generate printable diary PDF
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#include <math.h>
#include <stdio.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLSL(s) "#version 120\n" #s

static const int width = 3648;
static const int height = 5106;
static const float size = 5.0;

static const char *frag_src = GLSL(

  const float pi = 3.141592653589793;

  const float phi  = 1.618033988749895;
  const float phi1 = 0.618033988749895;
  vec3 a = -normalize(vec3(-phi,phi-1.0,1.0));
  vec3 b = -normalize(vec3(1.0,-phi,phi+1.0));
  vec3 c = -normalize(vec3(0.0,0.0,-1.0));

  uniform int polyhedron;
  uniform int orientation;
  uniform bool equirectangular;

  vec3 stereo(vec2 x) {
    return vec3(2.0 * x, dot(x, x) - 1.0) / (dot(x, x) + 1.0);
  }
  
  vec3 equir(vec2 x) {
    return vec3(cos(x.x) * sin(x.y), cos(x.x) * cos(x.y), sin(x.x));
  }
  
  void main() {
    vec2 p = gl_TexCoord[0].xy;
    vec3 q;
    if (equirectangular)
    {
      if (abs(p.x) <= pi/2.0 && abs(p.y) <= pi)
      {
        q = equir(p);
        if (orientation < 2 && polyhedron == 4)
        {
          float t = pi / 2.0; q *= mat3(cos(t), -sin(t), 0.0,  sin(t), cos(t), 0.0,  0.0, 0.0, 1.0);
        }
      }
      else
      {
        gl_FragColor = vec4(1.0);
        return;
      }
    }
    else
    {
      q = stereo(1.5 * p);
    }
    if (orientation > 1 && polyhedron == 4)
    {
      float t = pi / 2.0; q *= mat3(cos(t), -sin(t), 0.0,  sin(t), cos(t), 0.0,  0.0, 0.0, 1.0);
      t = atan(1.0, sqrt(2.0)); q *= mat3(1.0, 0.0, 0.0,  0.0, cos(t), -sin(t),  0.0, sin(t), cos(t));
    }
    if (orientation > 0 && polyhedron == 4)
    {
      float t = pi / 4.0; q *= mat3(cos(t), 0.0, -sin(t),  0.0, 1.0, 0.0,  sin(t), 0.0, cos(t));
    }
    if (orientation > 0 && polyhedron == 3)
    {
      float t = pi / 2.0; q *= mat3(cos(t), -sin(t), 0.0,  sin(t), cos(t), 0.0,  0.0, 0.0, 1.0);
      t = atan(1.0, sqrt(2.0)); q *= mat3(1.0, 0.0, 0.0,  0.0, cos(t), -sin(t),  0.0, sin(t), cos(t));
      t = pi / 4.0; q *= mat3(cos(t), 0.0, -sin(t),  0.0, 1.0, 0.0,  sin(t), 0.0, cos(t));
    }
    if (orientation > 1 && polyhedron == 5)
    {
      float t = asin(2.0/3.0)/2.0; q *= mat3(cos(t), -sin(t), 0.0,  sin(t), cos(t), 0.0,  0.0, 0.0, 1.0);
    }
    if (orientation > 0 && polyhedron == 5)
    {
      float t = atan(2.0)/2.0; q *= mat3(1.0, 0.0, 0.0,  0.0, cos(t), -sin(t),  0.0, sin(t), cos(t));
    }
    float dx = length(dFdx(q));
    float dy = length(dFdy(q));
    float d = min(dx, dy);
    float e = 0.0;
    if (polyhedron == 3 || polyhedron == 4)
    {
      q = abs(q);
      float r = max(q.x, max(q.y, q.z));
      q /= r;
      float mi = min(min(q.x, q.y), q.z);
      float ma = 1.0;
      float md = q.x + q.y + q.z - mi - ma;
      if (polyhedron == 3) e = min(md - mi, ma - md);
      if (polyhedron == 4) e = min(mi, ma - md);
    }
    else if (polyhedron == 5)
    {
      float t;
      q = abs(q);
      t = dot(q, a); if (t < 0.0) q -= 2.0 * t * a;
      t = dot(q, b); if (t < 0.0) q -= 2.0 * t * b;
      t = dot(q, c); if (t < 0.0) q -= 2.0 * t * c;
      t = dot(q, b); if (t < 0.0) q -= 2.0 * t * b;
      q = abs(q);
      t = dot(q, a); if (t < 0.0) q -= 2.0 * t * a;
      t = dot(q, b); if (t < 0.0) q -= 2.0 * t * b;
      t = dot(q, c); if (t < 0.0) q -= 2.0 * t * c;
      t = dot(q, b); if (t < 0.0) q -= 2.0 * t * b;

      vec3 f = vec3(length(q - a), length(q - b), length(q - c));
      float ma = max(max(f.x, f.y), f.z);
      float mi = min(min(f.x, f.y), f.z);
      float md = f.x + f.y + f.z - ma - mi;
      e = ma - md;
    }
    gl_FragColor = vec4(vec3(0.5 + 0.5 * smoothstep(2.0 * d, 4.0 * d, e)), 1.0);
  }

);

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

int main(int argc, char **argv) {

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("graphpaper");
  glewInit();

  GLint success;
  int prog = glCreateProgram();
  int frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, (const GLchar **) &frag_src, 0);
  glCompileShader(frag);
  glAttachShader(prog, frag);
  glLinkProgram(prog);
  glGetProgramiv(prog, GL_LINK_STATUS, &success);
  if (!success)
  {
    debug_shader(frag, GL_FRAGMENT_SHADER, frag_src);
    debug_program(prog);
    exit(1);
  }
  glUseProgram(prog);
  GLuint upolyhedron = glGetUniformLocation(prog, "polyhedron");
  GLuint uorientation = glGetUniformLocation(prog, "orientation");
  GLuint uequirectangular = glGetUniformLocation(prog, "equirectangular");

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

  glViewport(0, 0, width, height);
  glLoadIdentity();
  gluOrtho2D(0, 1, 1, 0);

  unsigned char *buffer = malloc(width * height);
  for (int p = 3; p <= 5; ++p) {
    for (int o = 0; o < 3; ++o) {
      if (p == 3 && o == 2)
        continue;
      if (p == 5 && o > 0)
        continue;
      for (int l = 0; l < 2; ++l) {
        glUniform1i(upolyhedron, p);
        glUniform1i(uorientation, o);
        glUniform1i(uequirectangular, l);
        glBegin(GL_QUADS); {
          float u, w;
          if (l == 0)
          {
            u = size / 1.6666666666666666;
            w = u * height / width;
            w /= 0.825223;
          }
          else
          {
            w = 3.141592653589793 * 851 / 651;
            u = w * width / height;
            u *= 0.825223;
          }
          glTexCoord2f( u / 0.889977, -w); glVertex2f(1, 0);
          glTexCoord2f( u / 0.889977,  w); glVertex2f(1, 1);
          glTexCoord2f(-u / 0.760470,  w); glVertex2f(0, 1);
          glTexCoord2f(-u / 0.760470, -w); glVertex2f(0, 0);
        } glEnd();
        glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, buffer);
        char fname[200];
        snprintf(fname, 100, "platonic-%d-%d-%d.pgm", p, o, l);
        FILE *f = fopen(fname, "wb");
        fprintf(f, "P5\n%d %d\n255\n", width, height);
        fflush(f);
        fwrite(buffer, width * height, 1, f);
        fflush(f);
        fclose(f);
      }
    }
  }

  free(buffer);
  glDeleteFramebuffers(1, &fbo);
  glDeleteTextures(1, &tex);
  glDeleteShader(frag);
  glDeleteProgram(prog);
  glutReportErrors();
  return 0;
}
