# diary

generate printable diary PDF

## 2019

currently hardcoded to 2019 week-per-view with images on right hand page

UK special dates

## usage

    ./diary.sh

requres OpenGL and various PDF tools

output in 2019-print.pdf

needs 2.1GB disk space

## cover

needs mask.png, shape.png (black on white/alpha, 4096x4096)
suitable images can be converted by hand from cover-page.tex
note that GIMP imports PDF at 4094x4094 for some unknown reason...

cover is stochastic, run it several times with different seeds and pick the
most aesthetic result.  seed is saved in the ppm header for reproducibility

invocation example for 16 cores / 2.5GB RAM / 6.7GB disk space:

    # these values control appearance
    c=1.04
    N=2
    f=0.99
    for seed in $(seq -w 1 16)
    do
      ./cover 14588 10216 $c $N $f 100$seed > 100$seed.ppm &
    done

## legal

diary -- generate printable diary PDF

Copyright (C) 2018  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


-- 
https://mathr.co.uk/diary
