#!/bin/bash

# diary -- generate printable diary PDF
# Copyright (C) 2018  Claude Heiland-Allen
# License GPL3+ <http://www.gnu.org/licenses/>

(
cat <<EOF
\\documentclass{article}
\\usepackage[paperwidth=6.08in,paperheight=8.51in,margin=20mm,right=40mm]{geometry}
\\usepackage{tabularx}
\\usepackage{booktabs}
\\usepackage{multirow}
\\usepackage{adjustbox}
\\usepackage{lmodern}
\\renewcommand{\\familydefault}{\\sfdefault}

\\newcolumntype{C}{ >{\\centering\\arraybackslash} m{11mm} }
\\newcolumntype{R}{ >{\\raggedleft\\arraybackslash} X }

\\begin{document}
\\pagestyle{empty}
EOF
(
  echo 2018 12 31 0 365 0
  weekday=1
  month=12
  yearday=1
  for days in 31 28 31 30  31 30 31 31  30 31 30 31
  do
    month=$(((month % 12) + 1))
    for day in $(seq 1 $days)
    do
      echo 2019 $month $day $weekday $yearday $((365 - yearday))
      weekday=$(((weekday + 1) % 7))
      yearday=$((yearday + 1))
    done
  done
  day=1
  month=1
  yearday=1
  while ((weekday != 0))
  do
    echo 2020 $month $day $weekday $yearday $((365 - yearday))
    weekday=$(((weekday + 1) % 7))
    yearday=$((yearday + 1))
    day=$((day + 1))
  done
) |
cat -n |
(
  week=1
  line1=0
  while [ "$line1" != "" ]
  do
    read line1 year1 month1 day1 weekday1 yearday1 endday1
    read line2 year2 month2 day2 weekday2 yearday2 endday2
    read line3 year3 month3 day3 weekday3 yearday3 endday3
    read line4 year4 month4 day4 weekday4 yearday4 endday4
    read line5 year5 month5 day5 weekday5 yearday5 endday5
    read line6 year6 month6 day6 weekday6 yearday6 endday6
    read line7 year7 month7 day7 weekday7 yearday7 endday7
    if [ "$line1" == "" ]
    then
      break
    fi
    special1=$(grep "^$month1 $day1 " < days.txt | cut -d\  -f 3-)
    special2=$(grep "^$month2 $day2 " < days.txt | cut -d\  -f 3-)
    special3=$(grep "^$month3 $day3 " < days.txt | cut -d\  -f 3-)
    special4=$(grep "^$month4 $day4 " < days.txt | cut -d\  -f 3-)
    special5=$(grep "^$month5 $day5 " < days.txt | cut -d\  -f 3-)
    special6=$(grep "^$month6 $day6 " < days.txt | cut -d\  -f 3-)
    special7=$(grep "^$month7 $day7 " < days.txt | cut -d\  -f 3-)
    case $month1 in
      1) monthname1=January;;
      2) monthname1=February;;
      3) monthname1=March;;
      4) monthname1=April;;
      5) monthname1=May;;
      6) monthname1=June;;
      7) monthname1=July;;
      8) monthname1=August;;
      9) monthname1=September;;
      10) monthname1=October;;
      11) monthname1=November;;
      12) monthname1=December;;
    esac
    case $month7 in
      1) monthname7=January;;
      2) monthname7=February;;
      3) monthname7=March;;
      4) monthname7=April;;
      5) monthname7=May;;
      6) monthname7=June;;
      7) monthname7=July;;
      8) monthname7=August;;
      9) monthname7=September;;
      10) monthname7=October;;
      11) monthname7=November;;
      12) monthname7=December;;
    esac
    if ((month1 != month7))
    then
      extramonthyear=" / $monthname7 $year7"
    else
      extramonthyear="\\phantom{/}"
    fi
    caption=$(cat -n images.txt | grep " $week[[:space:]]" | sed "s/^.*pgm \(.*\)$/\1/g")
    cat << EOF
\\noindent\\begin{tabularx}{\textwidth}[t]{C @{\\extracolsep{\\fill}} R}
\\multicolumn{2}{l}{\\Large $monthname1 $year1$extramonthyear} \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day1} & \\multirow[t]{3}{*}{\\normalsize $special1} \\\\ \\Large Mon & \\\\ \\scriptsize ($yearday1-$endday1) & \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day2} & \\multirow[t]{3}{*}{\\normalsize $special2} \\\\ \\Large Tue & \\\\ \\scriptsize ($yearday2-$endday2) & \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day3} & \\multirow[t]{3}{*}{\\normalsize $special3} \\\\ \\Large Wed & \\\\ \\scriptsize ($yearday3-$endday3) & \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day4} & \\multirow[t]{3}{*}{\\normalsize $special4} \\\\ \\Large Thu & \\\\ \\scriptsize ($yearday4-$endday4) & \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day5} & \\multirow[t]{3}{*}{\\normalsize $special5} \\\\ \\Large Fri & \\\\ \\scriptsize ($yearday5-$endday5) & \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day6} & \\multirow[t]{3}{*}{\\normalsize $special6} \\\\ \\Large Sat & \\\\ \\scriptsize ($yearday6-$endday6) & \\\\
\\vfill & \\\\ \\midrule \\Huge {\\bf $day7} & \\multirow[t]{3}{*}{\\normalsize $special7} \\\\ \\Large Sun & \\\\ \\scriptsize ($yearday7-$endday7) & \\\\
\\vfill & \\\\ \\midrule \\normalsize Week \newline $(((week - 1)%52+1)) & \\normalsize $caption
\\end{tabularx}
\\newpage
EOF
    week=$((week + 1))
  done
)
cat <<EOF
\\topskip0pt
\\vspace*{\\fill}
\\centering
\\normalsize mathr.co.uk/diary/2019
\\end{document}
EOF
) > diary.tex
pdflatex diary.tex
pdflatex title-page.tex
make
./platonic-solids
./square-log-polar-graph-paper
./triangle-log-polar-graph-paper
./square-conformal-graph-paper
./triangle-conformal-graph-paper
./log-log-graph-paper
./htile
for i in *.pgm
do
  pnmtotiff < $i > $i.tif
  tiff2pdf -x 600 -y 600 -z -o $i.tif.pdf $i.tif
done
images=$(cat images.txt | cut -d\  -f 1 | sed "s/$/.tif.pdf/g")
pdftk title-page.pdf $images cat output images.pdf
pdftk A=images.pdf B=diary.pdf shuffle A B output 2019-print.pdf
gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dColorImageDownsampleType=/Bicubic -dGrayImageDownsampleType=/Bicubic -dMonoImageDownsampleType=/Bicubic -sOutputFile=2019-web.pdf 2019-print.pdf
