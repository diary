/*
diary -- generate printable diary PDF
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#include <stdio.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

#define GLSL(s) "#version 130\n" #s

static const int width = 3648;
static const int height = 5106;
static const float size = 2.0;

static const char *frag_src = GLSL(

float cosh(float val)
{
	float tmp = exp(val);
	float cosH = (tmp + 1.0 / tmp) / 2.0;
	return cosH;
}

float tanh(float val)
{
	float tmp = exp(val);
	float tanH = (tmp - 1.0 / tmp) / (tmp + 1.0 / tmp);
	return tanH;
}

float sinh(float val)
{
	float tmp = exp(val);
	float sinH = (tmp - 1.0 / tmp) / 2.0;
	return sinH;
}

vec2 cMul(vec2 a, vec2 b) {
	return vec2( a.x*b.x -  a.y*b.y,a.x*b.y + a.y * b.x);
}

vec2 cPower(vec2 z, float n) {
	float r2 = dot(z,z);
	return pow(r2,n/2.0)*vec2(cos(n*atan(z.y, z.x)),sin(n*atan(z.y,z.x)));
}



vec2 cInverse(vec2 a) {
	return	vec2(a.x,-a.y)/dot(a,a);
}

vec2 cDiv(vec2 a, vec2 b) {
	return cMul( a,cInverse(b));
}

vec2 cExp(vec2 z) {
	return vec2(exp(z.x) * cos(z.y), exp(z.x) * sin(z.y));
}

vec2 cLog(vec2 a) {
	float b =  atan(a.y,a.x);
	if (b>0.0) b-=2.0*3.1415;
	return vec2(log(length(a)),b);
}

vec2 cSqr(vec2 z) {
	return vec2(z.x*z.x-z.y*z.y,2.*z.x*z.y);
}

vec2 cSin(vec2 z) {
	return vec2(sin(z.x)*cosh(z.y), cos(z.x)*sinh(z.y));
}

vec2 cCos(vec2 z) {
	return vec2(cos(z.x)*cosh(z.y), -sin(z.x)*sinh(z.y));
}

vec2 cPower2(vec2 z, vec2 a) {
	return cExp(cMul(cLog(z), a));
}

const float pi = 3.141592653;

uniform int p;
uniform int q;

float opp = acosh((cos(pi/float(q)) + cos(pi / float(p)))/ sin(pi/float(p)));
float adj = acosh(cos(pi/float(p)) / sin(pi/float(q)));
float hyp = acosh(cos(pi/float(p)) * cos(pi/float(q)) / (sin(pi/float(p)) * sin(pi/float(q))));

vec2 face = vec2(0.0, 0.0);
vec2 edge = vec2(adj, 0.0);
vec2 vertex = vec2(hyp * cos(pi / q), hyp * sin(pi / q));

float edist(vec2 z, vec2 w) { return length(z - w); }
float hdist(vec2 z, vec2 w) { return acosh(1.0 + 2.0 * dot(z - w, z - w) / ((1.0 - dot(z, z)) * (1.0 - dot(w, w)))); }
float h2e(float z) { return sqrt((cosh(z) - 1.0) / (cosh(z) + 1.0)); }
vec2 h2e(vec2 z) { return normalize(z) * h2e(length(z)); }
float e2h(float z) { return hdist(vec2(z, 0.0), vec2(0.0, 0.0)); }
vec2 e2h(vec2 z) { return normalize(z) * e2h(length(z)); }

vec4 moebius(vec2 z) { return vec4(z, 1.0, 0.0); }
vec2 unmoebius(vec4 z) { return cDiv(z.xy, z.zw); }
float mlength(vec4 z) { return length(z.xy) / length(z.zw); }

mat4 rotation(float a) {
  float c = cos(a);
  float s = sin(a);
  return mat4
    ( c, -s, 0.0, 0.0
    , s,  c, 0.0, 0.0
    , 0.0, 0.0, 1.0, 0.0
    , 0.0, 0.0, 0.0, 1.0
    );
}

mat4 translation(vec2 z) {
  float s = atan(z.y, z.x);
  float l = length(z);
  float e = exp(l);
  float f = e + 1.0;
  float g = e - 1.0;
  mat4 m = mat4
    ( f, 0.0, g, 0.0
    , 0.0, f, 0.0, g
    , g, 0.0, f, 0.0
    , 0.0, g, 0.0, f
    );
  if (l > 0.0) {
    return rotation(-s) * m * rotation(s);
  } else {
    return m;
  }
}

mat4 rotationAbout(vec2 z, float a) { return translation(z) * rotation(a) * translation(-z); }

vec3 circleBetween(vec2 p, vec2 q) {
  float ax = p.x;
  float ay = p.y;
  float a2 = ax * ax + ay * ay;
  float bx = q.x;
  float by = q.y;
  float b2 = bx * bx + by * by;
  float cx = ax / a2;
  float cy = ay / a2;
  float c2 = cx * cx + cy * cy;
  float d = 2.0 * (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by));
  float ux = (a2*(by - cy) + b2*(cy - ay) + c2*(ay - by)) / d;
  float uy = (a2*(cx - bx) + b2*(ax - cx) + c2*(bx - ax)) / d;
  float dx = ux - q.x;
  float dy = uy - q.y;
  float r = sqrt(dx * dx + dy * dy);
  return vec3(ux, uy, r);
}


// hyperbolic area D(r) = 4 pi sinh^2(r/2)
// angle deficit = - area
// pi = 4 pi sinh^2 (r / 2)
// r = 2 asinh (1/2)
// 2.0 pi / 3 = ... => r = 2 asinh(sqrt(1/6))
void main() {
  vec2 zz = gl_TexCoord[0].yx;
  vec2 z = zz.yx;
  vec2 z0 = vec2(0.0, 0.0);
  vec2 z1 = unmoebius
         ( translation(vertex) * rotation(pi/3.0) * translation(vertex)
         * translation(edge) * translation(edge)
         * moebius(z0)
         );
  float theta = atan(z1.x, z1.y);
  mat4 transformation[32];
  int k = 0;
  for (int f = 0; f < q; ++f) {
    vec2 vert = vec2(hyp * cos(pi * float(2 * f + 1) / float(q)), hyp * sin(pi * float(2 * f + 1) / float(q)));
    for (int v = 1; v < p; ++v) {
      transformation[k++] = rotationAbout(vert, 2.0 * pi * float(v) / float(p));
    }
  }
  float v = 1.0;
  if (abs(z.x) < pi/4.0) {
    z = cDiv(cSin(z), cCos(z));
    if (length(z) >= 1.0) { gl_FragColor = vec4(1.0); return; }
    vec4 m = rotation(theta) * moebius(z);
    for (int i = 0; i <12; ++i) {
      if (mlength(m) <= h2e(adj)) { break; }
      vec4 mm = m;
      bool done = true;
      for (int j = 0; j < 32 && j < k; ++j) {
        vec4 mmm = transformation[j] * m;
        if (mlength(mmm) < mlength(mm)) {
          mm = mmm;
          done = false;
        }
      }
      m = normalize((mm));
      if (done) { break; }
    }
    vec2 w = unmoebius(m);
    float a = atan(w.y, w.x) * float(2 * q) / (2.0 * pi);
    float b = atan(w.y, w.x) * float(1 * q) / (2.0 * pi);
    float d = (1.0 + abs(tan(2.0 * zz.y))) * 0.0002 / length(w);
    a -= floor(a);
    b -= floor(b);
    a = min(a, 1.0 - a);
    b = min(b, 1.0 - b);
    a = min(a, b);
    v = smoothstep(2.0 * d, 4.0 * d, a);
  }
  gl_FragColor = vec4(0.5 + 0.5 * v);
}

);

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

int main(int argc, char **argv) {

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("graphpaper");
  glewInit();

  GLint success;
  int prog = glCreateProgram();
  int frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, (const GLchar **) &frag_src, 0);
  glCompileShader(frag);
  glAttachShader(prog, frag);
  glLinkProgram(prog);
  glGetProgramiv(prog, GL_LINK_STATUS, &success);
  if (!success)
  {
    debug_shader(frag, GL_FRAGMENT_SHADER, frag_src);
    debug_program(prog);
    exit(1);
  }
  glUseProgram(prog);
  GLuint up = glGetUniformLocation(prog, "p");
  GLuint uq = glGetUniformLocation(prog, "q");

  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

  glViewport(0, 0, width, height);
  glLoadIdentity();
  gluOrtho2D(0, 1, 1, 0);

  unsigned char *buffer = malloc(width * height);
  int pq[9][2] =
    { { 3, 7 }
    , { 3, 8 }
    , { 4, 5 }
    , { 4, 6 }
    , { 5, 4 }
    , { 5, 5 }
    , { 6, 4 }
    , { 7, 3 }
    , { 8, 3 }
    };
  for (int k = 0; k < 9; ++k) {
        glUniform1i(up, pq[k][0]);
        glUniform1i(uq, pq[k][1]);
        glBegin(GL_QUADS); {
          float u = size / 2.0;
          float w = u * height / width / 0.825223;
          glTexCoord2f( u / 0.889977, -w); glVertex2f(1, 0);
          glTexCoord2f( u / 0.889977,  w); glVertex2f(1, 1);
          glTexCoord2f(-u / 0.760470,  w); glVertex2f(0, 1);
          glTexCoord2f(-u / 0.760470, -w); glVertex2f(0, 0);
        } glEnd();
        glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, buffer);
        char fname[200];
        snprintf(fname, 100, "htile-%d-%d.pgm", pq[k][0], pq[k][1]);
        FILE *f = fopen(fname, "wb");
        fprintf(f, "P5\n%d %d\n255\n", width, height);
        fflush(f);
        fwrite(buffer, width * height, 1, f);
        fflush(f);
        fclose(f);
  }

  free(buffer);
  glDeleteFramebuffers(1, &fbo);
  glDeleteTextures(1, &tex);
  glDeleteShader(frag);
  glDeleteProgram(prog);
  glutReportErrors();
  return 0;
}
